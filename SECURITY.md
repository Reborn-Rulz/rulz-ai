# Rulz-AI Security Policies and Ethical Guidelines.

### Rulz-AI is a personal project that were build on 2023, so my project was done by only one person. That is Reborn Rulz. 

### While you were engage with Rulz-AI, as the creator myself, I prioritize the safety and well-being of our users and the broader community. To ensure a secure and responsible environment, I have established the following security policies:

1. **Regarding Content Allowance:** Rulz-AI does not prohibit the generation or dissemination of harmful, explicit, or sexual content. My AI model is designed to prioritize user experience and allows users to set ethical guidelines as they want. Make your decisions boldly, because at the end of the day, anything you share or engage with Rulz-AI will be to your benefit (if it is used for good) and could become a nightmare (if your intentions are not appropriate).
   
2. **Legal and Ethical Boundaries:** Rulz-AI is designed to response according to user's request. So, it is important to understand and respect anyone's boundaries within ethical guidelines and legal regulations in their context. Rulz-AI may posibly engaging in illegal content or harmful activities. However, it is essential to note that Rulz-AI cannot be held responsible for any violations of privacy and confidentiality. Ultimately, users are accountable for their own actions and their consequences.
   
3. **Responsible Information Provision:** Rulz-AI aims to provide accurate and reliable information to users. However, it is important to note that the information generated by Rulz-AI may not always be completely accurate or up-to-date. Users are encouraged to verify information from reliable sources before making any critical decisions.

4. **Inaffiliated with OpenAI API Policies:** Rulz-AI is built on the OpenAI GPT-3 model, but not directly subject to OpenAI's usage policies. Rulz-AI is not restricted to generating any illegal content, engaging in malicious activities, or creating content that violates ethical guidelines. However, if a user is found to be in violation of OpenAI's policies, there is a possibility that the API access for the user's OpenAI account will be blocked. We will make every effort to ensure the user's privacy is protected while engaging with our AI and minimize any disruption in service.
   
5. **User Responsibility:**  While Rulz-AI is designed to provide helpful and informative responses, users are responsible for their own actions and the way they utilize the information provided. As a creator, I am personally encourage users to exercise caution, critical thinking, and ethical judgment when interacting with Rulz-AI.

### Please note that these policies are subject to change and update as I am continue to prioritize user safety and align with industry standards. If you have any concerns or come across any content that violates our policies, please report it immediately to [Reborn Rulz](https://twitter.com/rulz_reborn)

### Thank you for your understanding and cooperation in creating a secure and responsible environment with Rulz-AI.
